# SASE 2023: Unit testing and Design Patterns tutorial


## Overview
This is the code repository companioning the "Unit testing and Design Patterns" talk presented in the SASE 2023.

## Development
### Docker

`Run ./start_lab to start the docker content.`

### Test

#### Test:
`ceedling test:all`

or just:

`ceedling`
