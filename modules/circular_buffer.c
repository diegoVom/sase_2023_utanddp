
/* Copyright 2023, Pablo Pittaro - Diego Ezequiel Vommaro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]============================================*/
#include "circular_buffer.h"

/*==================[macros and definitions]================================*/

/*==================[internal data declaration]=============================*/

/*==================[internal functions declaration]========================*/

/*==================[internal data definition]==============================*/

/*==================[external data definition]==============================*/

/*==================[internal functions definition]=========================*/

/*==================[external functions definition]=========================*/
extern void circular_buffer_init(circular_buffer_t *me,
                                 uint8_t *sto,
                                 uint32_t len)
{
    me->sto = sto;
    me->index = 0;
    me->outdex = 0;
    me->size = len;
    me->count = 0;
}

extern uint8_t circular_buffer_isEmpty(circular_buffer_t *me)
{
    return (me->count == 0);
}

extern uint8_t circular_buffer_isFull(circular_buffer_t *me)
{
    return (me->count == me->size);
}

extern void circular_buffer_put(circular_buffer_t *me, uint8_t element)
{
    me->sto[me->index++] = element;
    me->count++;
   
    if (me->index == me->size)
    {
        me->index = 0;
    }
}

extern uint8_t circular_buffer_get(circular_buffer_t *me)
{
    uint8_t ret;

    me->count--;
    ret =  me->sto[me->outdex++];

    if (me->outdex == me->size)
    {
        me->outdex = 0;
    }

    return ret;
}
/*==================[end of file]===========================================*/

