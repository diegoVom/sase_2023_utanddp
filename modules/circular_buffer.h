
/* Copyright 2023, Pablo Pittaro - Diego Ezequiel Vommaro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef CIRCULAR_BUFFER_H__
#define CIRCULAR_BUFFER_H__

/*==================[inclusions]============================================*/
#include <stdint.h>

/*==================[cplusplus]=============================================*/
#ifdef __cplusplus
extern "C" {
#endif
#if 0
}
#endif

/*==================[macros]================================================*/

/*==================[typedef]===============================================*/
typedef struct circular_buffer_type
{
    uint8_t *sto;
    uint32_t size;
    uint32_t index;
    uint32_t outdex;
    uint32_t count;
}circular_buffer_t;

/*==================[external data declaration]=============================*/

/*==================[external functions declaration]========================*/
extern void circular_buffer_init(circular_buffer_t *me,
                                 uint8_t *sto,
                                 uint32_t len);
extern uint8_t circular_buffer_isEmpty(circular_buffer_t *me);
extern uint8_t circular_buffer_isFull(circular_buffer_t *me);
extern void circular_buffer_put(circular_buffer_t *me, uint8_t element);
extern uint8_t circular_buffer_get(circular_buffer_t *me);

/*==================[cplusplus]=============================================*/
#ifdef __cplusplus
}
#endif
/*==================[end of file]===========================================*/
#endif /* CIRCULAR_BUFFER_H__ */
