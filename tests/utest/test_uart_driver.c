
/* Copyright 2023, Pablo Pittaro - Diego Ezequiel Vommaro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "unity.h"
#include "uart_driver.h"


/**
 * UART driver
 * 1. Config baudrate, parity, enable fifo 
 * 2.
*/

extern UART_Driver_t uart;

void setUp(void)
{

}

void tearDown(void)
{

}

void test_hardware_registers_areSet()
{
    config_hardware(115200);

    TEST_ASSERT_EQUAL_UINT32(1, uart.FCR);
    TEST_ASSERT_EQUAL_UINT32(1, uart.LCR);
    TEST_ASSERT_EQUAL_UINT32(0x5, uart.DLL);
    TEST_ASSERT_EQUAL_UINT32(0, uart.DLM);
}

void test_uart_receives_when_LSR_isSet()
{
    char retValue;
    uart.RBR = 0x12;
    uart.LSR = 1;

    retValue = uart_receive_char();

    TEST_ASSERT_EQUAL_CHAR(0x12, retValue);
}

void test_uart_returns_0_when_LSR_isNotSet()
{
    char retValue;
    uart.RBR = 0x12;
    uart.LSR = 0;

    retValue = uart_receive_char();

    TEST_ASSERT_EQUAL_CHAR(0, retValue);
}

void test_uart_sends_char_and_isReceived()
{
    char sentByte = 0x13;
    uart.LSR = 1;
    
    uart_send_char(sentByte);
    uart.RBR = uart.THR;

    TEST_ASSERT_EQUAL_CHAR(sentByte, uart_receive_char());
}
