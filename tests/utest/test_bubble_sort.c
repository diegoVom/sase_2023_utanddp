
/* Copyright 2023, Pablo Pittaro - Diego Ezequiel Vommaro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifdef TEST

#include "unity.h"

#include "bubble_sort.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_bubble_sort_reverse_sorted_array(void)
{
    uint32_t arr[] = {5, 4, 3, 2, 1};
    uint32_t expected[] = {1, 2, 3, 4, 5};
    uint32_t size = sizeof(arr) / sizeof(arr[0]);

    bubble_sort(arr, size);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(expected, arr, 5);
}

void test_bubble_sort_random(void)
{
    uint32_t arr[] = {3, 1, 5, 4, 2};
    uint32_t expected[] = {1, 2, 3, 4, 5};
    uint32_t size = sizeof(arr) / sizeof(arr[0]);

    bubble_sort(arr, size);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(expected, arr, 5);
}

void test_bubble_sort_interesting_sequence(void)
{
    uint32_t arr[] = {5, 2, 9, 1, 5};
    uint32_t expected[] = {1, 2, 5, 5, 9};
    uint32_t size = sizeof(arr) / sizeof(arr[0]);

    bubble_sort(arr, size);
    TEST_ASSERT_EQUAL_UINT32_ARRAY(expected, arr, 5);
}

#endif // TEST
