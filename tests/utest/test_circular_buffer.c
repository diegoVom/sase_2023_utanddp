
/* Copyright 2023, Pablo Pittaro - Diego Ezequiel Vommaro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]============================================*/
/* DO NOT REMOVE THESE HEADER FILES. THEY WERE GENERATED AUTOMATICALLY */
#include "unity.h"

/* ------------------------------------------------------------------- */
/* USER HEADER FILES... */
#include <string.h>
#include "circular_buffer.h"

/*==================[macros and definitions]================================*/
#define CB_SIZE 10

/*==================[internal data declaration]=============================*/

/*==================[internal functions declaration]========================*/

/*==================[internal data definition]==============================*/
circular_buffer_t cb_;
circular_buffer_t *cb = &cb_;

uint8_t sto[CB_SIZE+1];

/*==================[external data definition]==============================*/

/*==================[internal functions definition]=========================*/

/*==================[external functions definition]=========================*/
void setUp()
{
    memset(sto, 0, sizeof (sto));
    circular_buffer_init(cb, sto, CB_SIZE);
}

void tearDown()
{
}

void test_ciruclarBuffer_Empty_after_init(void)
{
    TEST_ASSERT_EQUAL(1, circular_buffer_isEmpty(cb));
}

void test_put_should_addAnElement(void)
{
    circular_buffer_put(cb, 0x12);

    TEST_ASSERT_EQUAL(0, circular_buffer_isEmpty(cb));
}

void test_get_should_getAnElement(void)
{
    circular_buffer_put(cb, 0x12);
    TEST_ASSERT_EQUAL(0x12, circular_buffer_get(cb));
}

void test_get_and_put_aFewElements(void)
{
    circular_buffer_put(cb, 0x12);
    circular_buffer_put(cb, 0x34);
    circular_buffer_put(cb, 0x56);

    TEST_ASSERT_EQUAL(0x12, circular_buffer_get(cb));
    TEST_ASSERT_EQUAL(0x34, circular_buffer_get(cb));
    TEST_ASSERT_EQUAL(0x56, circular_buffer_get(cb));
}

void test_circular_buffer_NotFull_after_init(void)
{
    TEST_ASSERT_EQUAL(0, circular_buffer_isFull(cb));
}

void test_circular_buffer_full(void)
{
    uint32_t i;

    for (i = 0; i < CB_SIZE; i++)
    {
        circular_buffer_put(cb, i);
    }

    TEST_ASSERT_EQUAL(1, circular_buffer_isFull(cb));

}

void test_circular_buffer_fullToEmpty(void)
{
    uint32_t i;

    for (i = 0; i < CB_SIZE; i++)
    {
        circular_buffer_put(cb, i);
    }

    TEST_ASSERT_EQUAL(1, circular_buffer_isFull(cb));

    for (i = 0; i < CB_SIZE; i++)
    {
        TEST_ASSERT_EQUAL(i, circular_buffer_get(cb));
    }

    TEST_ASSERT_EQUAL(1, circular_buffer_isEmpty(cb));
}

void test_circular_buffer_fullNotFullThenFull(void)
{
    uint32_t i;

    for (i = 0; i < CB_SIZE; i++)
    {
        circular_buffer_put(cb, i);
    }

    TEST_ASSERT_EQUAL(1, circular_buffer_isFull(cb));

    TEST_ASSERT_EQUAL(0, circular_buffer_get(cb));

    circular_buffer_put(cb, CB_SIZE);

    for (i = 1; i <= CB_SIZE; i++)
    {
        TEST_ASSERT_EQUAL(i, circular_buffer_get(cb));
    }

    TEST_ASSERT_EQUAL(0, sto[CB_SIZE]);
}

/*==================[end of file]===========================================*/
