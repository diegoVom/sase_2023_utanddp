
/* Copyright 2023, Pablo Pittaro - Diego Ezequiel Vommaro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifdef TEST

#include "unity.h"

#include "protocol.h"
#include "mock_circular_buffer.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_given_emptyQueue_then_noMessage(void)
{
    msg_t msg = {0};
    msg_t msg_exp = {0};

    circular_buffer_isEmpty_ExpectAnyArgsAndReturn(1);
    TEST_ASSERT_FALSE(protocol_getMessage(&msg));
    TEST_ASSERT_EQUAL_MEMORY(&msg_exp, &msg, sizeof (msg));
}

void test_given_NotEmptyQueue_then_Message(void)
{
    msg_t msg;

    circular_buffer_isEmpty_ExpectAnyArgsAndReturn(0);
    circular_buffer_get_ExpectAnyArgsAndReturn(1);
    TEST_ASSERT_TRUE(protocol_getMessage(&msg));
}

void test_messageWithRightData(void)
{
    msg_t msg = {0};


    circular_buffer_isEmpty_ExpectAnyArgsAndReturn(0);
    circular_buffer_get_ExpectAnyArgsAndReturn(0x12);
    TEST_ASSERT_TRUE(protocol_getMessage(&msg));

    TEST_ASSERT_EQUAL(2, msg.x);
    TEST_ASSERT_EQUAL(1, msg.y);
}

#endif // TEST
